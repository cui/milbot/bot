let publishCmd = `
ACTION_SERVER_IMAGE=$CI_REGISTRY_IMAGE/action-server
BOT_CORE_IMAGE=$CI_REGISTRY_IMAGE/core

echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY --username $CI_REGISTRY_USER --password-stdin
docker build -f .docker/Dockerfile-action-server -t "$ACTION_SERVER_IMAGE:\${nextRelease.version}" -t "$ACTION_SERVER_IMAGE:latest" .
docker push --all-tags "$ACTION_SERVER_IMAGE"
docker build -f .docker/Dockerfile-bot-core -t "$BOT_CORE_IMAGE:\${nextRelease.version}" -t "$BOT_CORE_IMAGE:latest" .
docker push --all-tags "$BOT_CORE_IMAGE"
`
let config = require('semantic-release-preconfigured-conventional-commits');
config.branches = ["master", "main"];
config.plugins.push(
    [
        "@semantic-release/exec",
        {
            "publishCmd": publishCmd
        }
    ],
    "@semantic-release/gitlab",
    "@semantic-release/git"
)
module.exports = config