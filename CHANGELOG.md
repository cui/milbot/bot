## [1.1.0](https://gitlab.unige.ch/cui/milbot/bot/compare/1.0.0...1.1.0) (2023-11-26)


### Features

* **models:** add config for fetching remote model ([2c74f6d](https://gitlab.unige.ch/cui/milbot/bot/commit/2c74f6d81b6a0c6ed7360cbb69e3ddea003c2fa6))


### General maintenance

* **gitignore:** add models directory ([0d241f4](https://gitlab.unige.ch/cui/milbot/bot/commit/0d241f4a30f090066a71ea5921230ace82752988))
