# MILBOT

MILBOT - Chatbot to engage millenials with public media

### Local development/debug

- Clone the repository

- Create a network called `rasa` (iff the network does not exist already)
    ```
    docker network create rasa
    ```

- Build and start the containers
    ```
    docker compose build --no-cache
    docker compose up --force-recreate
    ```
