from rasa_sdk import Action
from rasa_sdk.events import SlotSet


class ResetSlotPostcode(Action):
    """Sets slotvalue to None"""

    def name(self):
        return "action_reset_slot_difficulte"

    def run(self, dispatcher, tracker, domain):
        return [SlotSet("difficulte", None)]
