from typing import Text, List, Optional

import pandas as pd
from rasa_sdk import FormValidationAction, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict


class QuizzForm(FormValidationAction):

    def name(self) -> Text:
        return "validate_quizz_form"

    async def required_slots(self,
                             slots_mapped_in_domain: List[Text],
                             dispatcher: "CollectingDispatcher",
                             tracker: "Tracker",
                             domain: "DomainDict") -> Optional[List[Text]]:
        additionnal_slots = []

        if tracker.slots.get("difficulte") == "Facile":
            # required_slots = slots_mapped_in_domain + list(quizz.Question)
            quizz = pd.read_csv("actions/quizzUsedB.csv", encoding="utf-8")
            additionnal_slots = list(quizz.Question)
        elif tracker.slots.get("difficulte") == "Dur":
            quizz = pd.read_csv("actions/quizzUsed.csv", encoding="utf-8")
            additionnal_slots = list(quizz.Question)
        return slots_mapped_in_domain + additionnal_slots

    # def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
    #    quizz = pd.read_csv("actions/quizzUsed.csv")
    #
    #    return{
    #        quizz.Question[0] : [
    #            self.from_intent(intent="correctAnswer", value ="Correct"),
    #            self.from_intent(intent="falseAnswer", value= f"{quizz.Answer[0]}")
    #        ],
    #        quizz.Question[1] : [
    #            self.from_intent(intent="correctAnswer", value="Correct"),
    #            self.from_intent(intent="falseAnswer", value=f"{quizz.Answer[1]}")
    #        ],
    #        quizz.Question[2]: [
    #            self.from_intent(intent="correctAnswer", value="Correct"),
    #            self.from_intent(intent="falseAnswer", value=f"{quizz.Answer[2]}")

    #        ],
    #        quizz.Question[3]: [
    #            self.from_intent(intent="correctAnswer", value="Correct"),
    #            self.from_intent(intent="falseAnswer", value= f"{quizz.Answer[3]}")

    #        ]
    #    }

    # def submit(self,
    #           dispatcher :CollectingDispatcher,
    #           tracker : Tracker, domain: Dict[Text,Any]) -> List[Dict]:
    #    dispatcher.utter_message(text = "Merci")
    #    return []
