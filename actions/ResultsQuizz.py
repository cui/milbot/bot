from typing import Any, Text, Dict, List

import pandas as pd
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ResultsQuizz(Action):

    def name(self) -> Text:
        return 'action_results'

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        slot_value = tracker.get_slot("difficulte")
        if slot_value == "Facile":
            quizz = pd.read_csv("actions/quizzUsedB.csv", encoding='utf-8')
        else:
            quizz = pd.read_csv("actions/quizzUsed.csv", encoding='utf-8')
        listQuestions = list(quizz.Question)
        listAnswer = list(quizz.Answer)
        listAnswerDisplay = []
        for a in range(len(listQuestions)):
            slot_value = tracker.get_slot(f"{listQuestions[a]}")
            if slot_value == "Correct":
                listAnswerDisplay.append(slot_value)
            else:
                listAnswerDisplay.append(listAnswer[a])
        data = [
            {"title": "question 1", "description": f"{listAnswerDisplay[0]}"},
            {"title": "question 2", "description": f"{listAnswerDisplay[1]}"},
            {"title": "question 3", "description": f"{listAnswerDisplay[2]}"},
            {"title": "question 4", "description": f"{listAnswerDisplay[3]}"}
        ]
        message = {"payload": "collapsible_quizz", "data": data}
        dispatcher.utter_message(text="Voici tes résultats", json_message=message)

        return []
