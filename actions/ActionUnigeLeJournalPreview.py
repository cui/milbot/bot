import json
from typing import Any, Text, Dict, List

import requests
from bs4 import BeautifulSoup
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ActionUnigeLeJournalPreview(Action):

    def name(self) -> Text:
        return 'action_unige_le_journal_preview'

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        id = "0"  # FIXME: to be passed to action
        file = open("actions/links.json")
        data = json.load(file)
        url = data["unige_le_journal_articles"][id]

        # Query Unige Le Journal Article
        # This will scrape Unige Le Journal website to get an article detail
        request = requests.get(url)

        # Scrape
        soup = BeautifulSoup(request.content, "html.parser")
        title = soup.find_all("div", id="blockStyle16830Main95")[0].text
        image = soup.find("div", id="blockStyle17060Main9").img["src"]
        image_url = "https://unige.ch{}".format(image)
        description = soup.find_all("div", id="blockStyle16843Main94")[0].text

        # Implement dispatcher
        dispatcher.utter_message(text=f"Voilà un article qui peut t'intéresser ! Le titre est le suivant : {title}")
        dispatcher.utter_message(image=f'{image_url}')
        dispatcher.utter_message(text=f'Et en bonus, un petit preview : \n {description}')
        dispatcher.utter_message(
            text=f"Hésite pas à faire un tour sur la page pour connaître la fin de l'histoire ! : \n {link}")

        return []
