import os
import requests
import time


def access_token():
    # Check if access token is still valid
    try:
        token_expiration = os.environ["token_expires_in"]
        token = os.environ["token"]

        if token_expiration > time.time():
            # Token expired
            return request_token()
        else:
            # Token not expired
            return token
    except:
        return request_token()


def request_token():
    request = requests.get('https://api.srgssr.ch/oauth/v1/accesstoken',
                           headers={
                               'Authorization': os.environ["CONSUMER_TOKEN"]},
                           params={'grant_type': 'client_credentials'})
    request = request.json()

    token_expiration = request["expires_in"]
    os.environ["token_expires_in"] = str(time.time() + token_expiration)

    token = request["access_token"]
    os.environ["token"] = token

    return token
