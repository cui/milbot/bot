import json
from typing import Any, Text, Dict, List

import requests
from bs4 import BeautifulSoup
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ActionRtsArticlePreview(Action):

    def name(self) -> Text:
        return 'action_rts_article_preview'

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        id = "0"  # FIXME: to be passed to action
        file = open("actions/links.json")
        data = json.load(file)
        url = data["rts_articles"][id]

        # Query RTS Article
        # This will scrape RTS Info website to get an article detail
        request = requests.get(url)

        # Scrape
        soup = BeautifulSoup(request.content, "html.parser")
        title = soup.find_all("h1", class_="article-title")[0].text
        image = soup.find_all("img", class_="embed-responsive-item")[0]["src"]
        description = soup.find_all("div", class_="article-lead")[0].text

        # Implement dispatcher
        dispatcher.utter_message(text=f"Voilà un article qui peut t'intéresser ! Le titre est le suivant : {title}")
        dispatcher.utter_message(image=f'{image}')
        dispatcher.utter_message(text=f'Et en bonus, un petit preview : \n {description}')
        dispatcher.utter_message(
            text=f"Hésite pas à faire un tour sur la page pour connaître la fin de l'histoire ! : \n {link}")

        return []
